//
// Created by Aleksei Ivanov on 2019-02-22.
//
#include <math.h>
#include <complex.h>
//#include <stdio.h>
typedef double complex double_complex;

double dot3(const double *x,const double *y);
void complex_to_real_mm_nc(const double_complex *x, const double_complex *y, double *out);
void complex_mm_cn(const double_complex *x, const double_complex *y, double_complex *out);
void complex_mm_nc(const double_complex *x, const double_complex *y, double_complex *out);
void complex_mm_nn(const double_complex *x, const double_complex *y, double_complex *out);

void egdecomp_skhm(const double *upp_tr, double_complex *eval_out, double_complex *evec_out);
void hadamard_prod_mv_c(const double_complex *x, const double_complex *y, double_complex *out);
void hadamard_prod_mm_comp(const double_complex *x, const double_complex *y, double_complex *out);


void skh2u(const double *upp_tr, double *out){

    // check if upp_tr zero then return unity matrix
    if (fabs(upp_tr[0]) < 1.0e-40 &&
        fabs(upp_tr[1]) < 1.0e-40 &&
        fabs(upp_tr[2]) < 1.0e-40){
        int k;
        int m = 0;
        for(k = 0; k < 3; k++){
            for(m = 0; m < 3; m++){
                if (m == k) out[3 * k + m] = 1.0;
                else out[3 * k + m] = 0.0;
            }
        }
        return;
    }

    double_complex eval[3];
    double_complex evec[9];
    double_complex exp_eval[3];

    double_complex evec_exp_ev[9];
    int i;

    egdecomp_skhm(upp_tr, eval, evec);

    for(i = 0; i < 3; i++){
        exp_eval[i] = cexp(eval[i]);
    }

    hadamard_prod_mv_c(evec, exp_eval, evec_exp_ev);
    complex_to_real_mm_nc(evec_exp_ev, evec, out);

}


void rodrigues_rotation(const double *upp_tr, double *out){

    /***
    upp_tr - vector x, y, z so that one calculate
    exp(A) with A= [[0, x, y],
                    [-x, 0, z],
                    [-y, -z, 0]]
    ***/


    if (fabs(upp_tr[0]) < 1.0e-40 &&
        fabs(upp_tr[1]) < 1.0e-40 &&
        fabs(upp_tr[2]) < 1.0e-40){
        int k;
        int m = 0;
        for(k = 0; k < 3; k++){
            for(m = 0; m < 3; m++){
                if (m == k) out[3 * k + m] = 1.0;
                else out[3 * k + m] = 0.0;
            }
        }
        return;
    }

    double theta = sqrt(upp_tr[0] * upp_tr[0] +
                        upp_tr[1] * upp_tr[1] +
                        upp_tr[2] * upp_tr[2]);

    double A = cos(theta);
    double B = sin(theta);
    double D = 1 - A;
    double x = upp_tr[0]/theta;
    double y = upp_tr[1]/theta;
    double z = upp_tr[2]/theta;

    // diagonal elements
    out[0] = A + z * z * D;
    out[4] = A + y * y * D;
    out[8] = A + x * x * D;

    // off diagonal
    double s1 = -y * z *D;
    double s2 = x * z * D;
    double s3 = -x * y * D;

    double a1 = x * B;
    double a2 = y * B;
    double a3 = z * B;

    out[1] = s1 + a1;
    out[3] = s1 - a1;
    out[2] = s2 + a2;
    out[6] = s2 - a2;
    out[5] = s3 + a3;
    out[7] = s3 - a3;

}

void egdecomp_skhm(const double *upp_tr, double_complex *eval_out, double_complex *evec_out){

    /*
     *  upp_tr is upper triangular part of 3 by 3 skew-symmetric matrix
     *  calculate its eigenvalues and store them in eval_out[0], eval_out[1], eval_out[3]
     *  calculate its eigenvectors in evec. the columns are eigenvertors
     */
    // check if upp_tr zero. If so return eval = 0 and evec unity
    if (fabs(upp_tr[0]) < 1.0e-40 &&
        fabs(upp_tr[1]) < 1.0e-40 &&
        fabs(upp_tr[2]) < 1.0e-40){
        int k;
        int m = 0;

        for(k = 0; k < 3; k++){
            eval_out[k] = 0.0;
            for(m = 0; m < 3; m++){
                if (m == k) evec_out[3 * k + m] = 1.0;
                else evec_out[3 * k + m] = 0.0;
            }
        }
        return;
    }

    // else continue this code

    double x = sqrt(dot3(upp_tr, upp_tr));
    double a = upp_tr[0];
    double b = upp_tr[1];
    double c = upp_tr[2];
    double norm;

    eval_out[0] = 0.0;
    eval_out[1] = -I * x;
    eval_out[2] = I * x;

    int i = 0;

    // first eigenvector
    evec_out[i] = c / x;
    evec_out[3 + i] = -b / x;
    evec_out[6 + i] = a / x;

    double ac = sqrt(a * a + c * c);

    i = 1;  // now consider second eigenvector
    if(ac < 1.0e-40){
        norm = sqrt(2.0);
        if(b < 0.0){
            evec_out[i] = -I / norm;
            evec_out[3 + i] = 0.0;
            evec_out[6 + i] = 1.0 / norm;
        }
        else{
            evec_out[i] = I / norm;
            evec_out[3 + i] = 0.0;
            evec_out[6 + i] = 1.0 / norm;
        }
    }
    else{
        norm = x * sqrt(2.0) * ac;
        evec_out[i] = (b * c + I * a * x) / norm;
        evec_out[3 + i] = (a * a + c * c) / norm;
        evec_out[6 + i] = (a * b - I * c * x) / norm;
    }

    // 3rd eigenvector is just complex conjugate of the 2nd.
    i = 2;
    evec_out[i] = conj(evec_out[i - 1]);
    evec_out[3 + i] = conj(evec_out[3 + i - 1]);
    evec_out[6 + i] = conj(evec_out[6 + i - 1]);

}


void get_gradients(const double *upp_tr, const double *m_uptr, double *out){

    double_complex T[9] = {0.0, m_uptr[2], -m_uptr[1],
                           -m_uptr[2], 0.0, m_uptr[0],
                           m_uptr[1], -m_uptr[0], 0.0};
    double_complex eval[3];
    double_complex evec[9];
    double_complex D_matrix[9];
    double_complex temp[9];
    double_complex temp2[9];
    int i;
    int j;

    egdecomp_skhm(upp_tr, eval, evec);
    complex_mm_nn(T, evec, temp);
    complex_mm_cn(evec, temp, temp2);

    // calculate D_matrix
    for (i = 0; i < 3; i++){
        for (j = 0; j < 3; j++){
            if (cabs((eval[i] - eval[j])) < 1.0e-40)
                D_matrix[3 * i + j] = 1.0;
            else
                D_matrix[3 * i + j] = (cexp((eval[i] - eval[j])) - 1.0) /
                                       (eval[i] - eval[j]);
        }
    }

    hadamard_prod_mm_comp(temp2, D_matrix, temp);


    complex_mm_nc(temp, evec, temp2);
    complex_mm_nn(evec, temp2, temp);

    out[0] = creal(*(temp + 1));
    out[1] = creal(*(temp + 2));
    out[2] = creal(*(temp + 3 + 2));

}