//
// Created by Aleksei Ivanov on 2019-02-25.
//

#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <stdbool.h>

#define DOUBLEP(a) ((double*)PyArray_DATA(a))
#define INTP(a) ((int*)PyArray_DATA(a))
#define COMPLEXP(a) ((double_complex*)PyArray_DATA(a))
#define CONSTDOUBLEP(a) ((const double*)PyArray_DATA(a))

void cross(double *x, double *y, double *out);
void calc_linear_equations(double *magn_mom, double *b, double dt, double *out);
double dot3(const double *x, const double *y);
void sigma_x_y(double *x, double *y,
               double D,double alpha,
               double h, int damping_only, double *out);

static PyObject* sib_step(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *spins_old;
    PyArrayObject *feild;
    PyArrayObject *random_field;

    PyObject *damping_only;
    PyObject *finite_temperature;
    PyObject *predictor;

    double alpha;
    double h;
    double D;

    if (!PyArg_ParseTuple(args, "OOOODDDOOO", &spins, &spins_old, &feild,
                          &random_field, &alpha, &h,  &D,
                          &damping_only, &finite_temperature, &predictor)
                          )
        return NULL;

    int damp = PyObject_IsTrue(damping_only);
    int finite_temp = PyObject_IsTrue(finite_temperature);
    int pred = PyObject_IsTrue(predictor);

    double *pspins = DOUBLEP(spins);
    double *pspins_old = DOUBLEP(spins_old);
    double *prandom_field = DOUBLEP(random_field);
    double *pfield = DOUBLEP(feild);

    double temp[3];
    double error = 0.0;
    double max_error = 0.0;

    int k;
    int i;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){

        // calculate torque:
        cross(pspins + 3 * k, pfield + 3 * k, temp);
        // calculate error:
        if (pred && (finite_temp == 0)){
            error = sqrt(dot3(temp, temp));
            if (max_error < error)
                max_error = error;
        }

        // calculate deterministic effective field:
        for(i = 0; i < 3; i++){
            if (damp) {
                pfield[3 * k + i] = alpha * temp[i];  // damping only
            }
            else
                pfield[3 * k + i] += alpha * temp[i];
        }

        if (finite_temp){
            // if finite temperature add thermal noise
            sigma_x_y(pspins + 3 * k, prandom_field + 3 * k, D,
                      alpha, h, damp, pfield + 3 * k);
        }

        // new value of the magnetic moment:
        if (pred){
            calc_linear_equations(pspins + 3 * k, pfield + 3 * k, h, temp);
            for (i = 0; i < 3; i++)
                pspins[3 * k + i] = (temp[i] + pspins[3 * k + i]) * 0.5;
        }
        else{
            calc_linear_equations(pspins_old + 3 * k, pfield + 3 * k, h, temp);
            for (i = 0; i < 3; i++)
                pspins[3 * k + i] = temp[i];
        }

    }

//    error /= (double) PyArray_DIMS(spins)[0];

    return Py_BuildValue("d", max_error);
}


static PyMethodDef DynMethods[] =
        {
                {"sib_step", sib_step, METH_VARARGS, ""},
                {NULL, NULL, 0, NULL}
        };


static struct PyModuleDef cModPyDem =
        {
                PyModuleDef_HEAD_INIT,
                "dyn_module", "Some documentation",
                -1,
                DynMethods
        };


PyMODINIT_FUNC
PyInit_dyn_module(void)
{
    import_array();
    return PyModule_Create(&cModPyDem);
}

