#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <complex.h>

typedef double complex double_complex;

#define DOUBLEP(a) ((double*)PyArray_DATA(a))
#define INTP(a) ((int*)PyArray_DATA(a))
#define COMPLEXP(a) ((double_complex*)PyArray_DATA(a))
#define CONSTDOUBLEP(a) ((const double*)PyArray_DATA(a))

double dot3(const double *x, const double *y);
void cross(const double *x, const double *y, double *out);
void calc_diff(double *x, double *y, double *out);
void get_offset_positions(double *x, double *offset,
                          double *cell, double *out);
void normalize(double *x);
int get_incriment(int k);
void skh2u(const double *upp_tr, double *out);
void egdecomp_skhm(const double *upp_tr, double_complex *eval_out, double_complex *evec_out);
void vm3(const double *m, const double *v, double *out);
void get_gradients(const double *upp_tr, const double *m_uptr, double *out);
void rodrigues_rotation(const double *upp_tr, double *out);

static PyObject* transform_skh2u(PyObject* self, PyObject* args){

    PyArrayObject *upp_tr;
    PyArrayObject *unit_m;

    if (!PyArg_ParseTuple(args, "OO", &upp_tr, &unit_m))
        return NULL;

    const double *upp_tr_part = CONSTDOUBLEP(upp_tr);
    double *out = DOUBLEP(unit_m);

    skh2u(upp_tr_part, out);

    Py_INCREF(Py_None);
	return Py_None;
}

static PyObject* calc_rodrigues_rotation(PyObject* self, PyObject* args){

    PyArrayObject *upp_tr;
    PyArrayObject *unit_m;

    if (!PyArg_ParseTuple(args, "OO", &upp_tr, &unit_m))
        return NULL;

    const double *upp_tr_part = CONSTDOUBLEP(upp_tr);
    double *out = DOUBLEP(unit_m);

    rodrigues_rotation(upp_tr_part, out);

    Py_INCREF(Py_None);
	return Py_None;
}

static PyObject* get_eigdc_skhm(PyObject* self, PyObject* args){

    PyArrayObject *upp_tr;
    PyArrayObject *eval_out;
    PyArrayObject *evec_out;

    if (!PyArg_ParseTuple(args, "OOO", &upp_tr, &eval_out, &evec_out))
        return NULL;

    const double *upp_tr_part = CONSTDOUBLEP(upp_tr);
    complex double *e_out = COMPLEXP(eval_out);
    complex double *v_out = COMPLEXP(evec_out);

    egdecomp_skhm(upp_tr_part, e_out, v_out);

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* rotate_all_spins(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *refs;
    PyArrayObject *upp_tr;

    if (!PyArg_ParseTuple(args, "OOO", &upp_tr, &refs, &spins))
        return NULL;

    const double *pupp_tr = CONSTDOUBLEP(upp_tr);
    const double *prefs = CONSTDOUBLEP(refs);
    double *pspins = DOUBLEP(spins);
    int k;

    double unit_m[9];

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        // skh2u(pupp_tr + 3 * k, unit_m);
        rodrigues_rotation(pupp_tr + 3 * k, unit_m);
        vm3(unit_m, prefs + 3 * k, pspins + 3 * k);
    }

    Py_INCREF(Py_None);
    return Py_None;

}


static PyObject* get_gradient_vector(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *upp_tr;
    PyArrayObject *eff_field;
    PyArrayObject *out;

    double error = 0.0;

    if (!PyArg_ParseTuple(args, "OOOO", &upp_tr, &spins, &eff_field, &out))
        return NULL;

    const double *pupp_tr = CONSTDOUBLEP(upp_tr);
    const double *pspins = CONSTDOUBLEP(spins);
    const double *peff = CONSTDOUBLEP(eff_field);
    double *pout = DOUBLEP(out);

    int k;

    double torque[3];
    double max_error = 0.0;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        cross(pspins + 3 * k, peff + 3 * k, torque);
        error = sqrt(dot3(torque, torque));
        if (max_error < error)
            max_error = error;
        get_gradients(pupp_tr + 3 * k, torque, pout + 3 * k);
    }

    return Py_BuildValue("d", max_error);
}


static PyObject* get_approx_gradient_vector(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *upp_tr;
    PyArrayObject *eff_field;
    PyArrayObject *out;

    double error = 0.0;

    if (!PyArg_ParseTuple(args, "OOOO", &upp_tr, &spins, &eff_field, &out))
        return NULL;

//    const double *pupp_tr = CONSTDOUBLEP(upp_tr);
    const double *pspins = CONSTDOUBLEP(spins);
    const double *peff = CONSTDOUBLEP(eff_field);
    double *pout = DOUBLEP(out);

    int k;

    double torque[3];
    double max_error = 0.0;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        cross(pspins + 3 * k, peff + 3 * k, torque);
        error = sqrt(dot3(torque, torque));
        if (max_error < error)
            max_error = error;
        *(pout + 3 * k) = torque[2];
        *(pout + 3 * k + 1) = -torque[1];
        *(pout + 3 * k + 2) = torque[0];

//        get_gradients(pupp_tr + 3 * k, torque, pout + 3 * k);
    }

    return Py_BuildValue("d", max_error);
}

static PyMethodDef UoMethods[] =
{
        {"transform_skh2u", transform_skh2u, METH_VARARGS, ""},
        {"get_eigdc_skhm", get_eigdc_skhm, METH_VARARGS, ""},
        {"rotate_all_spins", rotate_all_spins, METH_VARARGS, ""},
        {"get_gradient_vector", get_gradient_vector, METH_VARARGS, ""},
        {"get_approx_gradient_vector", get_approx_gradient_vector, METH_VARARGS, ""},
        {"calc_rodrigues_rotation", calc_rodrigues_rotation, METH_VARARGS, ""},
        {NULL, NULL, 0, NULL}
};


static struct PyModuleDef cModPyDem =
{
    PyModuleDef_HEAD_INIT,
    "uo_module", "Some documentation",
    -1,
    UoMethods
};


PyMODINIT_FUNC
PyInit_uo_module(void)
{
    import_array();
    return PyModule_Create(&cModPyDem);
}
