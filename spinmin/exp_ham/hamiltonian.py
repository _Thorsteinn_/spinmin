from ase.neighborlist import NeighborList, NewPrimitiveNeighborList
from spinmin.energy_and_gradients import *
import numpy as np
import energy_module
import gradient_module


class SpinHamiltonian:

    def __init__(self, atoms, spins, interactions=None, expnt_type='pair'):

        print('Initialization...')

        self.spins = spins
        self.atoms = atoms
        self.type = 'single_image'

        self.interactions = interactions
        self.expnt_type = expnt_type

        self.J = interactions.get('J')
        self.epsilon = interactions.get('eps')

        assert len(self.J) == len(self.epsilon)
        assert isinstance(self.J, list) or isinstance(self.J, float)

        # generate list of neighbours
        p0 = atoms.positions[0]
        dist = np.asarray(
            [np.linalg.norm(p0 - p) for p in atoms.positions])
        inds = np.argsort(dist)
        new_dist = [0.0]
        new_inds = [0]
        for j in inds:
            if (dist[j] - new_dist[-1]) < 1.0e-2:
                continue
            else:
                new_dist.append(dist[j])
                new_inds.append(j)

        if isinstance(self.J, float):
            self.J = np.asarray([self.J])
        self.J = np.asarray(self.J)
        my_ind = new_inds[1:len(self.J) + 1]
        self.nndist = dist[my_ind]
        self.fourspin_sums = np.zeros(4)
        self.r_c = self.nndist[-1]

        # neighbours
        print('Building neighbour list...')

        self.nbl_one_way = NeighborList(self.r_c, skin=0.01,
                                        self_interaction=False,
                                        bothways=False,
                                        primitive=NewPrimitiveNeighborList)

        self.nbl_both_way = NeighborList(self.r_c, skin=0.01,
                                         self_interaction=False,
                                         bothways=True,
                                         primitive=NewPrimitiveNeighborList)

        self.nbl_one_way.update(atoms)
        self.nbl_both_way.update(atoms)

        self.nbl1_ow = []
        self.nbl2_ow = []
        self.offsets_ow = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_one_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_ow.append(i)
                self.nbl2_ow.append(j)
                self.offsets_ow.append(z.tolist())
        self.nbl1_ow = np.array(self.nbl1_ow)
        self.nbl2_ow = np.array(self.nbl2_ow)
        self.offsets_ow = np.asarray(self.offsets_ow, dtype=float)

        self.nbl1_bw = []
        self.nbl2_bw = []
        self.offsets_bw = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_both_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_bw.append(i)
                self.nbl2_bw.append(j)
                self.offsets_bw.append(z.tolist())
        self.nbl1_bw = np.array(self.nbl1_bw)
        self.nbl2_bw = np.array(self.nbl2_bw)
        self.offsets_bw = np.asarray(self.offsets_bw, dtype=float)
        print('Finished building neighbour list...')

        self.energy_efham = 0.0

        # calculate length of spins and normalize it
        self.mm = []
        for i, s in enumerate(self.spins):
            x = np.sqrt(np.dot(s, s))
            s /= x
            if i > 0:
                if abs(x - self.mm[-1]) > 1.0e-6:
                    raise ValueError('Only spins with equal'
                                     ' length are accepted')
            self.mm.append(x)

        print('Initialization done')

    def get_energy_and_gradients(self):

        atoms, spins = self.atoms, self.spins
        grad = np.zeros(shape=(spins.shape[0], 3))

        cell = atoms.get_cell()
        if not isinstance(cell, np.ndarray):
            cell = cell.array

        nndist = self.nndist

        cell = atoms.get_cell()
        if not isinstance(cell,np.ndarray):
            cell = cell.array

        nnJ = np.asarray(self.J)
        epsilon = np.asarray(self.epsilon)

        if self.expnt_type == 'pair':
            self.energy_efham = gradient_module.exponentail_grad(
                spins, self.nbl1_bw, self.nbl2_bw,
                atoms.positions, self.offsets_bw, cell,
                nndist, nnJ, epsilon, grad)
        elif self.expnt_type == 'sum':
            self.energy_efham = gradient_module.exponentail_sum_grad(
                nnJ, epsilon, spins,
                self.nbl1_bw, self.nbl2_bw,
                nndist, atoms.positions,
                self.offsets_bw, cell, grad)
        else:
            raise KeyError

        self.e_total = self.energy_efham

        return self.e_total, grad
