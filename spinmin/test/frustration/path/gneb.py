from spinmin.gneb.gneb import GNEB
from spinmin.utilities import from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from ase import io

Js = [5.536086992003163,
      -1.4015137884323228,
      -0.36054595272654055,
      0.06210704103925329,
      0.022230489875200665,
      -0.00017424244886522468,
      -0.0019278176404764298,
      -0.0013801201075828403]

interactions={'J': Js
              }

# first minimum
atoms_i = io.read('../init.traj')
spins_i = atoms_i.get_initial_magnetic_moments()
ham_i = SpinHamiltonian(atoms_i, spins_i, interactions)

# second minimum
atoms_f = io.read('../ferromagn.traj')
spins_f = atoms_f.get_initial_magnetic_moments()
ham_f = SpinHamiltonian(atoms_f, spins_f, interactions)


gneb = GNEB()
gneb.interpolate_chain(ham_i, ham_f, noi=12)
gneb.plot_chain()
gneb.plot_interpolation()

from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
opt = UnitaryOptimisation(gneb, max_iter=1000, convergence=1.0e-7)
opt.run()

gneb.plot_chain(name='FinalFig')
gneb.plot_interpolation(name='FinalInterpolation')

gneb.write('final_path_flat.traj')
