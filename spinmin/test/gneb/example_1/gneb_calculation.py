from spinmin.gneb.gneb import GNEB
from spinmin.utilities import from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io


a = 1.0
# define interaction
interactions = {'J': 3.68*2,
                'DM': {'ampl': 1.39*2,
                       'dir': [0.0, 0.0, 1.0]},
                'Z_ad': {'ampl': from_mT_to_meV(3, 4000),
                         'dir': [0.0, 0.0, 1.0]},
                'K_ad': {'ampl': 0.7,
                         'dir': [0.0, 0.0, 1.0]}}

# reads in the first minimum
atoms_i = io.read('init_state_skyrm.traj')
spins_i = atoms_i.get_initial_magnetic_moments()
ham_i = SpinHamiltonian(atoms_i, spins_i, interactions)

# reads in the second minimum
atoms_f = io.read('final_state_skyrm.traj')
spins_f = atoms_f.get_initial_magnetic_moments()
ham_f = SpinHamiltonian(atoms_f, spins_f, interactions)

# initialise the GNEB class
gneb = GNEB()

# creates a chain of images with number of images (noi) and linearly interpolates the spins

gneb.interpolate_chain(ham_i, ham_f, noi=8)

# initialises the Unitary Optimisation and runs the calculation for the minimum energy path
opt = UnitaryOptimisation(gneb, max_iter=30000)

opt.run()

# this is example of how to run gneb with SIB

# from spinmin.dynamics.dynamics import SpinDynamics
# opt = SpinDynamics(gneb, dt=200, damping_only=True, n_steps=10000)
# opt.run()

# plots the energy relative to the initial state as a function of the reaction coordinates

gneb.plot_interpolation(name='Skyrmion_Annihilation')

# plots the Images
gneb.plot_chain('FinalImage')

# saves the chain as a list of Atom objects
gneb.write('final_path_skyrm.traj')
