from spinmin.gneb.gneb import GNEB
from spinmin.utilities import from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from ase import io

a = 2.51
interactions={'J': 6.44 * 2,
              'DM': {'ampl': 1.2 * 2,
                     'dir': [0.0, 0.0, 1.0]},
              'Z_ad': {'ampl': from_mT_to_meV(3, 1500),
                       'dir': [0.0, 0.0, 1.0]},
              'K_ad': {'ampl': 0.8,
                       'dir': [0.0, 0.0, 1.0]},
              'r_c': a/2+0.1}

# add the file you want to read in
atoms = io.read('final_path.traj', index=':')

gneb = GNEB()
# add the list pf Atom objects and the interactions
# to set up the chain in the gneb class
gneb.read(atoms, interactions=interactions)

from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from spinmin.minimise.searchdir import QuickMin
opt = UnitaryOptimisation(gneb,
                          searchdir_algo=QuickMin(dt=0.005),
                          linesearch_algo='UnitStep',
                          update_ref_spins_counter=10000,
                          max_iter=1000,
                          approx_grad=True,
                          convergence=1.0e-5)

opt.run()


# this is example of how to run gneb with SIB
# from spinmin.dynamics.dynamics import SpinDynamics
# opt = SpinDynamics(gneb, dt=200, damping_only=True, n_steps=10000)
# opt.run()

gneb.plot_chain(name='FinalFig')
gneb.plot_interpolation(name='High_Res')
