from ase.build import bcc100, hcp0001
from spinmin.effective_ham.cross_dot import SpinHamiltonian
import numpy as np
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
import matplotlib.pyplot as plt
import time
from spinmin.utilities import collinear_state, skyrm_function, plot_xy
from ase.visualize import view
from scipy.interpolate import interp1d
a = 1.0
N = 12
atoms = hcp0001('Fe', a=a, orthogonal=False, size=(N, N, 1))

spins = skyrm_function(atoms, 10.0*a)
# spins = collinear_state(len(atoms))
atoms.set_initial_magnetic_moments(spins)

ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': [0.1, 0.9],
                                    'T': [0.2, 0.4],
                                    'eps': [2.0, 4.0]})

# print(ham.get_energy_and_gradients()[0])
# print(np.allclose(
#     ham.get_energy_and_gradients()[0], -5.477076921575494))

opt = UnitaryOptimisation(ham,
                          convergence=1.0e-7)
g1, g2 = opt.calc_numerical()
print(np.allclose(g1, g2))
print(np.max(np.abs(g1-g2)))
