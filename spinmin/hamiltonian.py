from ase.neighborlist import NeighborList, NewPrimitiveNeighborList
from spinmin.energy_and_gradients import *
import numpy as np
import energy_module
import gradient_module


class SpinHamiltonian:

    def __init__(self, atoms, spins, interactions=None, tags=None):

        print('Initialization...')

        msg = "Your spins array is not contiguous, " \
              "please converte them to contiguous arrays" \
              "(see numpy docs)"
        assert spins.flags['C_CONTIGUOUS'], msg

        self.spins = spins
        self.atoms = atoms
        self.type = 'single_image'

        self.interactions = interactions

        # self.r_c = [interactions['r_c']] * len(atoms)
        self.J = interactions.get('J')
        self.DM = interactions.get('DM')
        self.K_ad = interactions.get('K_ad')
        self.K2_ad = interactions.get('K2_ad')
        self.Z_ad = interactions.get('Z_ad')

        self.I24 = interactions.get('I24')
        self.I34 = interactions.get('I34')
        self.I44 = interactions.get('I44')
        self.K44 = interactions.get('K44')
        self.B3 = interactions.get('B3')
        self.T1 = interactions.get('T1')

        self.B2 = interactions.get('B2')

        trilat_ints = \
            {'I24': self.I24,
             'I34': self.I34,
             'I44': self.I44,
             'K44': self.K44,
             'B3': self.B3,
             'T1': self.T1}

        for x in trilat_ints.keys():
            if trilat_ints[x] is not None:
                print('WARNING: ')
                print(x, ' wroks only for specific triangular lattices!')


        assert isinstance(self.J, list) or isinstance(self.J, float)

        # generate list of neighbours
        p0 = atoms.positions[0]
        dist = np.asarray(
            [np.linalg.norm(p0 - p) for p in atoms.positions])
        inds = np.argsort(dist)
        new_dist = [0.0]
        new_inds = [0]
        for j in inds:
            if (dist[j] - new_dist[-1]) < 1.0e-2:
                continue
            else:
                new_dist.append(dist[j])
                new_inds.append(j)

        if isinstance(self.J, float):
            self.J = np.asarray([self.J])
        self.J = np.asarray(self.J)
        my_ind = new_inds[1:len(self.J) + 1]
        self.nndist = dist[my_ind]
        self.pair_interaction_sums = np.zeros(len(self.J))
        self.fourspin_sums = np.zeros(4)
        self.r_c = self.nndist[-1]

        if self.B2 is not None:
            if isinstance(self.B2, float):
                self.B2 = np.asarray([self.B2])
            self.B2 = np.asarray(self.B2)
            assert len(self.B2) == len(self.J)
            self.biquadr_interaction_sums = np.zeros(len(self.B2))

        # neighbours
        print('Building neighbour list...')

        self.nbl_one_way = NeighborList(self.r_c, skin=0.01,
                                        self_interaction=False,
                                        bothways=False,
                                        primitive=NewPrimitiveNeighborList)

        self.nbl_both_way = NeighborList(self.r_c, skin=0.01,
                                         self_interaction=False,
                                         bothways=True,
                                         primitive=NewPrimitiveNeighborList)

        self.nbl_one_way.update(atoms)
        self.nbl_both_way.update(atoms)

        self.nbl1_ow = []
        self.nbl2_ow = []
        self.offsets_ow = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_one_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_ow.append(i)
                self.nbl2_ow.append(j)
                self.offsets_ow.append(z.tolist())
        self.nbl1_ow = np.array(self.nbl1_ow)
        self.nbl2_ow = np.array(self.nbl2_ow)
        self.offsets_ow = np.asarray(self.offsets_ow, dtype=float)

        self.nbl1_bw = []
        self.nbl2_bw = []
        self.offsets_bw = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_both_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_bw.append(i)
                self.nbl2_bw.append(j)
                self.offsets_bw.append(z.tolist())
        self.nbl1_bw = np.array(self.nbl1_bw)
        self.nbl2_bw = np.array(self.nbl2_bw)
        self.offsets_bw = np.asarray(self.offsets_bw, dtype=float)
        print('Finished building neighbour list...')

        self.energy_x = 0.0
        self.energy_dm = 0.0
        self.energy_anis = 0.0
        self.energy_anis2 = 0.0
        self.energy_zee = 0.0

        self.e_24 = 0.0
        self.e_34 = 0.0
        self.e_44 = 0.0
        self.e_k44 = 0.0
        self.e_b3 = 0.0

        self.e_t1 = 0.0
        self.e_b2 = 0.0

        # calculate length of spins and normalize it
        self.mm = []
        for i, s in enumerate(self.spins):
            x = np.sqrt(np.dot(s, s))
            s /= x
            if i > 0:
                if abs(x - self.mm[-1]) > 1.0e-6:
                    raise ValueError('Only spins with equal'
                                     ' length are accepted')
            self.mm.append(x)

        self.tags=tags
        self.eg_counter=0
        print('Initialization done')

    def get_energy_and_gradients(self):

        self.eg_counter+=1
        atoms, spins = self.atoms, self.spins
        grad = np.zeros(shape=(spins.shape[0], 3))

        cell = atoms.get_cell()
        if not isinstance(cell, np.ndarray):
            cell = cell.array

        nnJ = np.asarray(self.J)
        nndist = self.nndist
        # self.pair_interaction_sums[:] = np.zeros(len(nnJ))

        if self.J is not None:
            grad_tmp = np.zeros_like(grad)
            gradient_module.xnn_grad(
                spins, self.nbl1_bw, self.nbl2_bw,
                atoms.positions, self.offsets_bw, cell,
                nndist, nnJ, grad_tmp
            )
            energy_x = np.sum(spins * grad_tmp)/2

            # TODO: you need to calculate pair_interaction_sums
            # in grad module which will also return the energy then
            # the energy is calculated here only for
            # pair_interaction_sums

            self.pair_interaction_sums[:] = np.zeros(len(nnJ))
            self.energy_x = energy_module.xnn_energy(
                spins, self.nbl1_ow, self.nbl2_ow,
                atoms.positions, self.offsets_ow, cell,
                self.pair_interaction_sums, nndist, nnJ
            )

            if not np.allclose(energy_x, self.energy_x):
                raise Exception('Bug in x energy or grad !')

            grad += grad_tmp

        if self.DM is not None:
            grad_tmp = np.zeros_like(grad)
            if type(self.DM) is float:
                D = self.DM
                bloch = 0
                perp_v = np.array([0.0, 0.0, 0.0])
            else:
                D, perp_v = \
                    self.DM['ampl'], np.asarray(self.DM['dir'])
                bloch = 1
            gradient_module.dmgrad(D, spins, self.nbl1_bw,
                                   self.nbl2_bw, nndist,
                                   atoms.positions,
                                   self.offsets_bw, cell,
                                   grad_tmp, perp_v, bloch)
            self.energy_dm = np.sum(spins * grad_tmp)/2
            grad += grad_tmp

        if self.K_ad is not None:
            grad_tmp = np.zeros_like(grad)
            K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
            gradient_module.angrad(K, e_c, spins, grad_tmp)
            self.energy_anis = np.sum(spins * grad_tmp) / 2
            grad += grad_tmp

        if self.K2_ad is not None:
            grad_tmp = np.zeros_like(grad)
            K, e_c = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])
            gradient_module.angrad(K, e_c, spins, grad_tmp)
            self.energy_anis2 = np.sum(spins * grad_tmp) / 2
            grad += grad_tmp

        if self.Z_ad is not None:
            grad_tmp = np.zeros_like(grad)
            Z, e_c = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
            gradient_module.zgrad(Z, e_c, grad_tmp)
            self.energy_zee = np.sum(spins * grad_tmp)
            grad += grad_tmp

        if self.I24 is not None and self.I24 != 0.0:
            # en24, grad_tmp = calc_i24_energy_and_grad(self.I24, spins, len(atoms))
            # self.e_total += en24
            # grad[:] += grad_tmp
            # del grad_tmp

            grad_tmp = np.zeros_like(grad)
            self.e_24 = gradient_module.i24grad(self.I24, spins, grad_tmp)
            grad[:] += grad_tmp
            del grad_tmp
            self.fourspin_sums[0] = -self.e_24/self.I24
        else:
            self.fourspin_sums[0] = 0.0

        if self.I34 is not None and self.I34 != 0.0:
            # en34, grad_tmp = calc_i34_energy_and_grad(self.I34, spins, len(atoms))
            # self.e_total += en34
            # grad[:] += grad_tmp
            # del grad_tmp

            # print('I34 start\n')
            grad_tmp = np.zeros_like(grad)
            self.e_34 = gradient_module.i34grad(self.I34, spins, grad_tmp)
            grad[:] += grad_tmp
            del grad_tmp
            self.fourspin_sums[1] = -self.e_34/self.I34
        else:
            self.fourspin_sums[1] = 0.0

        if self.I44 is not None and self.I44 != 0.0:
            # en44, grad_tmp = calc_i44_energy_and_grad(self.I44, spins, len(atoms))
            # print('Py = ', en44)
            # self.e_total += en44
            # grad[:] += grad_tmp

            grad_tmp = np.zeros_like(grad)
            self.e_44 = gradient_module.i44grad(self.I44, spins, grad_tmp)
            grad[:] += grad_tmp
            self.fourspin_sums[2] = -self.e_44/self.I44
        else:
            self.fourspin_sums[2] = 0.0

        if self.K44 is not None and self.K44 != 0.0:
            # en44, grad_tmp = calc_i44_energy_and_grad(self.I44, spins, len(atoms))
            # print('Py = ', en44)
            # self.e_total += en44
            # grad[:] += grad_tmp

            grad_tmp = np.zeros_like(grad)
            self.e_k44 = gradient_module.k44grad(self.K44, spins, grad_tmp)
            grad[:] += grad_tmp
            self.fourspin_sums[3] = -self.e_k44/self.K44
        else:
            self.fourspin_sums[3] = 0.0

        if self.T1 is not None and self.T1 != 0.0:
            cell = atoms.get_cell()
            if not isinstance(cell,np.ndarray):
                cell = cell.array

            grad_tmp = np.zeros_like(grad)
            self.e_t1 = gradient_module.t1grad(self.T1, spins, self.nbl1_bw,
                                   self.nbl2_bw, nndist,
                                   atoms.positions,
                                   self.offsets_bw, cell,
                                   grad_tmp)
            grad[:] += grad_tmp

        if self.B3 is not None and self.B3 != 0.0:
            # en24, grad_tmp = calc_i24_energy_and_grad(self.I24, spins, len(atoms))
            # self.e_total += en24
            # grad[:] += grad_tmp
            # del grad_tmp

            grad_tmp = np.zeros_like(grad)
            self.e_b3 = gradient_module.b3grad(self.B3, spins, grad_tmp)
            grad[:] += grad_tmp
            del grad_tmp

        if self.B2 is not None:
            self.biquadr_interaction_sums[:] = np.zeros(len(self.B2))
            grad_tmp = np.zeros_like(grad)
            self.e_b2 = gradient_module.biquadratic_grad(
                spins, self.nbl1_bw, self.nbl2_bw,
                atoms.positions, self.offsets_bw, cell,
                nndist, self.B2, grad, self.biquadr_interaction_sums)
            grad[:] += grad_tmp
            del grad_tmp

        self.e_total = self.energy_x + \
                        self.energy_dm + \
                        self.energy_anis + \
                        self.energy_anis2 + \
                        self.energy_zee + \
                        self.e_24 + \
                        self.e_34 + \
                        self.e_44 + \
                        self.e_k44 + \
                        self.e_t1 + \
                        self.e_b3 + \
                        self.e_b2

        if self.tags is not None:
            grad[self.tags] = np.zeros_like(grad[self.tags])

        return self.e_total, grad
    #
    # def get_energy(self):
    #
    #     atoms, spins = self.atoms, self.spins
    #
    #     cell = atoms.get_cell()
    #     if not isinstance(cell, np.ndarray):
    #         cell = cell.array
    #
    #     nnJ = np.asarray(self.J)
    #     nndist = self.nndist
    #     self.pair_interaction_sums[:] = np.zeros(len(nnJ))
    #
    #     if self.J is not None:
    #         self.energy_x = energy_module.xnn_energy(
    #             spins, self.nbl1_ow, self.nbl2_ow,
    #             atoms.positions, self.offsets_ow, cell,
    #             self.pair_interaction_sums, nndist, nnJ
    #         )
    #
    #         # self.energy_heiss = calc_heis_energy(self.J, spins,
    #         #                                      self.nbl_one_way)
    #         #
    #         # self.energy_heiss = energy_module.xenergy(self.J, spins,
    #         #                                           self.nbl1_ow,
    #         #                                           self.nbl2_ow)
    #
    #     if self.DM is not None:
    #
    #         # self.energy_dm = calc_dm_energy(self.DM, atoms, spins,
    #         #                                 self.nbl_one_way)
    #
    #         if type(self.DM) is float:
    #             D = self.DM
    #             bloch = 0
    #             perp_v = np.array([0.0, 0.0, 0.0])
    #         else:
    #             D, perp_v = \
    #                 self.DM['ampl'], np.asarray(self.DM['dir'])
    #             bloch = 1
    #
    #         cell = atoms.get_cell()
    #         if not isinstance(cell,np.ndarray):
    #             cell = cell.array
    #         self.energy_dm = energy_module.dmenergy(D, spins,
    #                                                 self.nbl1_ow,
    #                                                 self.nbl2_ow,
    #                                                 nndist,
    #                                                 atoms.positions,
    #                                                 self.offsets_ow,
    #                                                 cell,
    #                                                 perp_v, bloch
    #                                                 )
    #     if self.K_ad is not None:
    #         # self.energy_anis = calc_anisotropy_energy(self.K_ad,
    #         #                                           spins)
    #         K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
    #         self.energy_anis = energy_module.anenergy(K, e_c, spins)
    #     if self.K2_ad is not None:
    #         # self.energy_anis = calc_anisotropy_energy(self.K_ad,
    #         #                                           spins)
    #         K, e_c = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])
    #         self.energy_anis2 = energy_module.anenergy(K, e_c, spins)
    #
    #     if self.Z_ad is not None:
    #         # self.energy_zee = calc_zeeman_energy(self.Z_ad, spins)
    #         Z, e_c = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
    #         self.energy_zee = energy_module.zenergy(Z, e_c, spins)
    #
    #     self.e_total = self.energy_x + \
    #                    self.energy_dm + \
    #                    self.energy_anis + \
    #                    self.energy_anis2 + \
    #                    self.energy_zee
    #
    #     return self.e_total
    #
    # def get_gradients(self):
    #
    #     atoms, spins = self.atoms, self.spins
    #     grad = np.zeros(shape=(spins.shape[0], 3))
    #
    #     cell = atoms.get_cell()
    #     if not isinstance(cell, np.ndarray):
    #         cell = cell.array
    #
    #     nnJ = np.asarray(self.J)
    #     nndist = self.nndist
    #     # self.pair_interaction_sums[:] = np.zeros(len(nnJ))
    #
    #     if self.J is not None:
    #         gradient_module.xnn_grad(
    #             spins, self.nbl1_bw, self.nbl2_bw,
    #             atoms.positions, self.offsets_bw, cell,
    #             nndist, nnJ, grad
    #         )
    #         # grad += calc_heis_grad(self.J, spins, self.nbl_both_way)
    #         # gradient_module.xgrad(self.J, spins, self.nbl1_bw,
    #         #                       self.nbl2_bw, grad)
    #
    #     if self.DM is not None:
    #         if type(self.DM) is float:
    #             D = self.DM
    #             bloch = 0
    #             perp_v = np.array([0.0, 0.0, 0.0])
    #         else:
    #             D, perp_v = \
    #                 self.DM['ampl'], np.asarray(self.DM['dir'])
    #             bloch = 1
    #
    #         # grad += calc_dm_grad(self.DM, atoms, spins,
    #         #                      self.nbl_both_way)
    #         cell = atoms.get_cell()
    #         if not isinstance(cell,np.ndarray):
    #             cell = cell.array
    #
    #         gradient_module.dmgrad(D, spins, self.nbl1_bw,
    #                                self.nbl2_bw, nndist,
    #                                atoms.positions,
    #                                self.offsets_bw, cell,
    #                                grad, perp_v, bloch)
    #     if self.K_ad is not None:
    #         # grad += calc_anisotropy_grad(self.K_ad, spins)
    #         K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
    #         gradient_module.angrad(K, e_c, spins, grad)
    #     if self.K2_ad is not None:
    #         # grad += calc_anisotropy_grad(self.K_ad, spins)
    #         K, e_c = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])
    #         gradient_module.angrad(K, e_c, spins, grad)
    #     if self.Z_ad is not None:
    #         # grad += calc_zeeman_grad(self.Z_ad, spins.shape[0])
    #         Z, e_c = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
    #         gradient_module.zgrad(Z, e_c, grad)
    #
    #     if self.I24 is not None and self.I24 != 0.0:
    #         # en24, grad_tmp = calc_i24_energy_and_grad(self.I24, spins, len(atoms))
    #         # self.e_total += en24
    #         # grad[:] += grad_tmp
    #         # del grad_tmp
    #
    #         grad_tmp = np.zeros_like(grad)
    #         self.e_24 = gradient_module.i24grad(self.I24, spins, grad_tmp)
    #         self.e_total += self.e_24
    #         grad[:] += grad_tmp
    #         del grad_tmp
    #         self.fourspin_sums[0] = -self.e_24/self.I24
    #     else:
    #         self.fourspin_sums[0] = 0.0
    #
    #     if self.I34 is not None and self.I34 != 0.0:
    #         # en34, grad_tmp = calc_i34_energy_and_grad(self.I34, spins, len(atoms))
    #         # self.e_total += en34
    #         # grad[:] += grad_tmp
    #         # del grad_tmp
    #
    #         # print('I34 start\n')
    #         grad_tmp = np.zeros_like(grad)
    #         self.e_34 = gradient_module.i34grad(self.I34, spins, grad_tmp)
    #         self.e_total += self.e_34
    #         grad[:] += grad_tmp
    #         del grad_tmp
    #         self.fourspin_sums[1] = -self.e_34/self.I34
    #
    #     else:
    #         self.fourspin_sums[1] = 0.0
    #
    #     if self.I44 is not None and self.I44 != 0.0:
    #         # en44, grad_tmp = calc_i44_energy_and_grad(self.I44, spins, len(atoms))
    #         # print('Py = ', en44)
    #         # self.e_total += en44
    #         # grad[:] += grad_tmp
    #
    #         grad_tmp = np.zeros_like(grad)
    #         self.e_44 = gradient_module.i44grad(self.I44, spins, grad_tmp)
    #         self.e_total += self.e_44
    #         grad[:] += grad_tmp
    #         self.fourspin_sums[2] = -self.e_44/self.I44
    #     else:
    #         self.fourspin_sums[2] = 0.0
    #
    #     if self.K44 is not None and self.K44 != 0.0:
    #         # en44, grad_tmp = calc_i44_energy_and_grad(self.I44, spins, len(atoms))
    #         # print('Py = ', en44)
    #         # self.e_total += en44
    #         # grad[:] += grad_tmp
    #
    #         grad_tmp = np.zeros_like(grad)
    #         self.e_k44 = gradient_module.k44grad(self.K44, spins, grad_tmp)
    #         self.e_total += self.e_k44
    #         grad[:] += grad_tmp
    #         self.fourspin_sums[3] = -self.e_k44/self.K44
    #     else:
    #         self.fourspin_sums[3] = 0.0
    #
    #     if self.T1 is not None and self.T1 != 0.0:
    #         cell = atoms.get_cell()
    #         if not isinstance(cell,np.ndarray):
    #             cell = cell.array
    #
    #         grad_tmp = np.zeros_like(grad)
    #         self.e_t1 = gradient_module.t1grad(self.T1, spins, self.nbl1_bw,
    #                                self.nbl2_bw, nndist,
    #                                atoms.positions,
    #                                self.offsets_bw, cell,
    #                                grad_tmp)
    #         self.e_total += self.e_t1
    #         grad[:] += grad_tmp
    #
    #
    #     if self.B3 is not None and self.B3 != 0.0:
    #         # en24, grad_tmp = calc_i24_energy_and_grad(self.I24, spins, len(atoms))
    #         # self.e_total += en24
    #         # grad[:] += grad_tmp
    #         # del grad_tmp
    #
    #         grad_tmp = np.zeros_like(grad)
    #         self.e_b3 = gradient_module.b3grad(self.B3, spins, grad_tmp)
    #         self.e_total += self.e_b3
    #         grad[:] += grad_tmp
    #         del grad_tmp
    #
    #     return grad
    #