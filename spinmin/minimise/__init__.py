from .searchdir import SteepestDescent, FRcg, HZcg, PRPcg, PRcg,\
    QuickMin, LBFGS, LBFGS2, LSR1P, FlexVPO
from .linesearch import UnitStepLength, StrongWolfeConditions,\
    Parabola, CutOff, AWC


def search_direction(method):
    if isinstance(method, str):
        method = {'name': method}

    if isinstance(method, dict):
        kwargs = method.copy()
        name = kwargs.pop('name')
        if name == 'SD':
            return SteepestDescent()
        elif name == 'FRcg':
            return FRcg()
        elif name == 'HZcg':
            return HZcg()
        elif name == 'PRPcg':
            return PRPcg()
        elif name == 'PRcg':
            return PRcg()
        elif name == 'QuickMin':
            return QuickMin()
        elif name == 'LBFGS':
            return LBFGS(**kwargs)
        elif name == 'LBFGS2':
            return LBFGS2(**kwargs)
        elif name == 'LSR1P':
            return LSR1P(**kwargs)
        elif name == 'FlexVPO':
            return FlexVPO(**kwargs)
        else:
            raise ValueError('Check keyword for search direction!')
    elif isinstance(method, (SteepestDescent, FRcg, HZcg, PRPcg,
                             PRcg, QuickMin, LBFGS)):
        return method
    else:
        raise ValueError('Check keyword for search direction!')


def line_search_algorithm(method, objective_function, sda):

    if isinstance(method, str):
        method = {'name': method}

    if isinstance(method, dict):
        kwargs = method.copy()
        name = kwargs.pop('name')
        if name == 'UnitStep':
            return UnitStepLength(objective_function)
        elif name == 'Parabola':
            return Parabola(objective_function)
        elif name == 'SwcAwc':
            kwargs['sda'] = sda
            return StrongWolfeConditions(objective_function,
                                         **kwargs
                                         )
        elif name == 'Awc':
            return AWC(objective_function)
        elif name == 'CutOff':
            return CutOff(objective_function, **kwargs)
        else:
            raise ValueError('Check keyword for line search!')
    else:
        raise ValueError('Check keyword for line search!')
